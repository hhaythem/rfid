# Instructions pour exécuter l’application :

## Serveur Mysql:
    - Lancer le serveur Mysql et vérifié s'il écoute sur le port '3308', sinon il faut changer le port dans le fichier de configuration de la connection à la base de donnèes 'database'
## Base Mysql:
    - Créer la base de données avec le script SQL disponible dans le fichier "sql\database.sql" 
## Installation des dépendances:
    - Accédez à la racine du projet depuis le terminal et exécutez la commande "npm install" pour installer les dépendances du projet.
## Exécution de l'application:
    - Depuis le terminal exécutez la commande "npm start", pour lancer le serveur express qui écoutera sur le port 3050 si aucun autre port est configuré dans l'environnement.
## Tester L'Api:
    - Pour tester L'Api voici quelques requêtes à envoyer en POST depuis le : http://localhost:3050/tag

###     Test 1 : retourne une erreur de validation dans la console
        {
            "epc": "E98A25982730000848952365",
            "antenna": 3,
            "rssi": -100
        }
###    Test 2 : Insertion d’un nouveau tag
        {
            "epc": "E98A25982-30000/48952365",
            "antenna": 3,
            "rssi": -10
        }

###    Test 3 : envoyer la même requête du test2 pour vérifier l'intervalle de 10 secondes pour les paires (EPC, Antenna)identiques.