import { createPool, Pool } from 'mysql2/promise'

export async function connect(): Promise<Pool>{
    const connection = await createPool({
        host: 'localhost',
        user: 'root',
        password:'',
        database: 'etk',
        port: 3308,
        connectionLimit: 10
    });
    return connection;
}