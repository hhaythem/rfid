import { Router } from 'express';
const router = Router();


import { indexRfid } from "../controllers/index.controller";

router.route('/')
    .get(indexRfid);

export default router;