import { Request, Response } from "express";
import { connect } from '../database';

export async function indexRfid(req: Request, res: Response){
    const conn  = await connect();
    const tags = await conn.query('SELECT * FROM tagread');
    return res.json(tags);
}