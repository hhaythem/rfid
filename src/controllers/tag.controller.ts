import { Request, Response } from 'express';
import { connect } from '../database';
import { Tag } from "../interface/Tag.interface";
import { validation } from '../validation/tag.validation';

export async function createTag(req: Request, res: Response) {
    
    const newTag: Tag = req.body;
    newTag.timestampreader = new Date().getTime();
    newTag.timestamprecv = newTag.timestampreader;
    const errors = validation(newTag);
    if(errors){
        console.log(errors);
        return res.status(400).json();
    }
    const conn = await connect();
    const checkPair:any[]  = await conn.query("SELECT * FROM tagread WHERE (epc LIKE  ? AND antenna =  ?)  AND ((ROUND((? - timestamprecv)/1000))<10)", [newTag.epc, newTag.antenna, newTag.timestamprecv]);
    

    if(checkPair[0].length>0){
        return res.json({message: `Intervalle de 10 secondes pour la paire (${newTag.epc}, ${newTag.antenna}) doit être respecter`});
    }
    await conn.query("INSERT INTO `tagread` SET ?", [newTag]); 
    return res.json({message: "inserted"});
    
    
   




    
    

}


