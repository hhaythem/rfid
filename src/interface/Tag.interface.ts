export interface Tag{
    id?: string;
    epc: string;
    antenna: number;
    rssi: number;
    timestampreader: number;
    timestamprecv: number;
}    