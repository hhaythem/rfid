import express , {Application} from 'express';
import morgan from 'morgan';


//routes
import indexRoutes from "./routes/index.routes";
import tagRoutes from "./routes/tag.routes";

export class App{
    private app: Application;

    constructor(private port?: number | string){
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
    }

    settings(){
        this.app.set('port', this.port || process.env.port || 3050);
    }

    middlewares(){
        this.app.use(morgan('dev'));
        this.app.use(express.json());
    }


    routes(){
      this.app.use(indexRoutes);
      this.app.use('/tag',tagRoutes);  
    }

    async listen(){
        await this.app.listen(this.app.get('port'));
        console.log('le serveur utilise le port ', this.app.get('port'));
    }
}