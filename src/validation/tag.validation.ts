import { Tag } from "../interface/Tag.interface";
import  Joi  from '@hapi/joi';

export function validation(tagToValidate: Tag){
    const schema =  Joi.object().keys({
            epc: Joi.string().length(24).regex(/^E98A25.*[0-9]{8}$/).required(),
            antenna : Joi.number().integer().min(1).max(4).required(),
            rssi : Joi.number().integer().min(-70).max(0).required(),
            timestampreader : Joi.date().timestamp('unix').required(),
            timestamprecv : Joi.date().timestamp('unix').required()
        });
        const { value, error } = schema.validate(tagToValidate);
        if(error && error.details){           
            return error;
        }
    }

