CREATE DATABASE etk;

CREATE TABLE tagread(
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    epc varchar(25) COLLATE utf8_unicode_ci NOT NULL,
    antenna int(11) NOT NULL,
    rssi int(11) NOT NULL,
    timestampreader bigint(20) NOT NULL,
    timestamprecv bigint(20) NOT NULL,    
);

DESCRIBE tagread;