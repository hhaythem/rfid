"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("@hapi/joi"));
function validation(tagToValidate) {
    const schema = joi_1.default.object().keys({
        epc: joi_1.default.string().length(24).regex(/^E98A25.*[0-9]{8}$/).required(),
        antenna: joi_1.default.number().integer().min(1).max(4).required(),
        rssi: joi_1.default.number().integer().min(-70).max(0).required(),
        timestampreader: joi_1.default.date().timestamp('unix').required(),
        timestamprecv: joi_1.default.date().timestamp('unix').required()
    });
    const { value, error } = schema.validate(tagToValidate);
    if (error && error.details) {
        return error;
    }
}
exports.validation = validation;
