"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
const tag_validation_1 = require("../validation/tag.validation");
async function createTag(req, res) {
    const newTag = req.body;
    newTag.timestampreader = new Date().getTime();
    newTag.timestamprecv = newTag.timestampreader;
    const errors = tag_validation_1.validation(newTag);
    if (errors) {
        console.log(errors);
        return res.status(400).json();
    }
    const conn = await database_1.connect();
    const checkPair = await conn.query("SELECT * FROM tagread WHERE (epc LIKE  ? AND antenna =  ?)  AND ((ROUND((? - timestamprecv)/1000))<10)", [newTag.epc, newTag.antenna, newTag.timestamprecv]);
    if (checkPair[0].length > 0) {
        return res.json({ message: `Intervalle de 10 secondes pour la paire (${newTag.epc}, ${newTag.antenna}) doit être respecter` });
    }
    await conn.query("INSERT INTO `tagread` SET ?", [newTag]);
    return res.json({ message: "inserted" });
}
exports.createTag = createTag;
