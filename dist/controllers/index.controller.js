"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
async function indexRfid(req, res) {
    const conn = await database_1.connect();
    const tags = await conn.query('SELECT * FROM tagread');
    return res.json(tags);
}
exports.indexRfid = indexRfid;
