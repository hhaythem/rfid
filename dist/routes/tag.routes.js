"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const tag_controller_1 = require("../controllers/tag.controller");
router.route('/')
    .post(tag_controller_1.createTag);
exports.default = router;
