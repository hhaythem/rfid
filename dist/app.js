"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
//routes
const index_routes_1 = __importDefault(require("./routes/index.routes"));
const tag_routes_1 = __importDefault(require("./routes/tag.routes"));
class App {
    constructor(port) {
        this.port = port;
        this.app = express_1.default();
        this.settings();
        this.middlewares();
        this.routes();
    }
    settings() {
        this.app.set('port', this.port || process.env.port || 3050);
    }
    middlewares() {
        this.app.use(morgan_1.default('dev'));
        this.app.use(express_1.default.json());
    }
    routes() {
        this.app.use(index_routes_1.default);
        this.app.use('/tag', tag_routes_1.default);
    }
    async listen() {
        await this.app.listen(this.app.get('port'));
        console.log('le serveur utilise le port ', this.app.get('port'));
    }
}
exports.App = App;
